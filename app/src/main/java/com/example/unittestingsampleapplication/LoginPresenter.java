package com.example.unittestingsampleapplication;

import java.util.regex.Pattern;

public class LoginPresenter {
    private LoginPresenterCallBack mLoginPresenterCallBack;
    final static String ENTER_USER_NAME_ERROR = "ERROR: Please enter user name";
    final static String ENTER_PASSWORD_ERROR = "ERROR: Please enter Password";
    final static String INVALID_USER = "ERROR: Invalid user name";

    public String handleLogin(String userName, String password, LoginWebserviceIntf loginWebserviceManager, final LoginPresenterCallBack loginPresenterCallBack) {
        this.mLoginPresenterCallBack = loginPresenterCallBack;
        if (userName.isEmpty()) {
            return ENTER_USER_NAME_ERROR;
        } else if (!userName.isEmpty() && password.isEmpty()) {
            return ENTER_PASSWORD_ERROR;
        } else if (!userName.isEmpty() && !password.isEmpty()) {
            // check if userName is email type
            if (isValidEmail(userName)) {
                String response = loginWebserviceManager.authenticateUser(userName, password);
                if (response.equalsIgnoreCase(LoginWebserviceManager.LOGIN_SUCCESS)) {
                    mLoginPresenterCallBack.onSuccessFullLogin();
                } else if (response.equalsIgnoreCase(LoginWebserviceManager.LOGIN_FAILED)) {
                    mLoginPresenterCallBack.onLoginFailure();
                }
            } else {
                return INVALID_USER;
            }
        }
        return "";
    }

    private static boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public interface LoginPresenterCallBack {
        void onSuccessFullLogin();

        void onLoginFailure();
    }
}
