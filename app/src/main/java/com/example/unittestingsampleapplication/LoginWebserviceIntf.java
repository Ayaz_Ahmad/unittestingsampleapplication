package com.example.unittestingsampleapplication;

public interface LoginWebserviceIntf {
    String authenticateUser(String userName, String password);
}
