package com.example.unittestingsampleapplication;

import android.os.Handler;

public class LoginWebserviceManager implements LoginWebserviceIntf {
    final static String LOGIN_SUCCESS = "Login Success";
    final static String LOGIN_FAILED = "Login Failed";

    @Override
    public String authenticateUser(String userName, String password) {
        // do a webservice hit and return result
        return LOGIN_SUCCESS;
    }
}
