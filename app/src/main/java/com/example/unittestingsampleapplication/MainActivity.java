package com.example.unittestingsampleapplication;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.button);
        final TextInputEditText userNameEditText = findViewById(R.id.userNameEditText);
        final TextInputEditText passwordEditText = findViewById(R.id.passwordEditText);
        final ProgressBar progressBar = findViewById(R.id.progressBar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginPresenter loginPresenter = new LoginPresenter();
                String message = loginPresenter.handleLogin(userNameEditText.getEditableText().toString(), passwordEditText.getEditableText().toString(), new LoginWebserviceManager(),
                        new LoginPresenter.LoginPresenterCallBack() {
                            @Override
                            public void onSuccessFullLogin() {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Login Successfull", Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onLoginFailure() {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(getApplicationContext(), "Login Failed Try Again ", Toast.LENGTH_LONG).show();
                            }
                        });

                if (!message.isEmpty()) {
                    Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                }
            }
        });

    }
}
