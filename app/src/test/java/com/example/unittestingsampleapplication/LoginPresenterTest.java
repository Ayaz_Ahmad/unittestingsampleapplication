package com.example.unittestingsampleapplication;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

public class LoginPresenterTest {
    private LoginPresenter mLoginPresenter;

    @Mock
    private LoginWebserviceManager loginWebserviceManager;
    private LoginPresenter.LoginPresenterCallBack loginPresenterCallBack;

    @Before
    public void setUp() {
        loginWebserviceManager = Mockito.mock(LoginWebserviceManager.class);
        loginPresenterCallBack = Mockito.mock(LoginPresenter.LoginPresenterCallBack.class);
        mLoginPresenter = new LoginPresenter();
    }

    @Test
    public void testHandleLoginForEmptyUserName() {
        String message = mLoginPresenter.handleLogin("", "kajdbckjd", null, null);
        Assert.assertEquals(LoginPresenter.ENTER_USER_NAME_ERROR, message);
    }

    @Test
    public void testHandleLoginWithWebserviceSuccess() {
        Mockito.when(loginWebserviceManager.authenticateUser("jksknj@kjsnv.com", "sdvsvsv")).thenReturn(LoginWebserviceManager.LOGIN_SUCCESS);
        mLoginPresenter.handleLogin("jksknj@kjsnv.com", "sdvsvsv", loginWebserviceManager, loginPresenterCallBack);
        Mockito.verify(loginPresenterCallBack).onSuccessFullLogin();
    }

    @Test
    public void testHandleLoginWithWebserviceFailure() {
        //TODO
    }

    @After
    public void inTheEnd() {
        mLoginPresenter = null;
    }
}
